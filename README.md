# TP-Pokemon

This is pokemon

## Question

    1 - Non

    2 - Dans la class Pokemon la fonction soigner() ne respecte pas la reponsabilité unique

    3 - Non, le diagramme ne respecte pas le principe Open/Close

    4 - On ne retrouve pas ce principe dans le diagramme car si un pokémon attaque un autre il faudrait modifier la fonction attaquer()
    en vérifiant le type de la cible. Donc en cas d’ajout de type cela viendras modifier les classes Bulbizarre, Salamèche et Carapuce.

    5 - Non, le diagramme ne respecte pas le principe de substitution de Liskov

    6 - Car en cas d’ajout de Type les classes Carapuce, Salamèche et Bulbizarre seront altérés

    7 - Non

    8 - Car il y a que deux Interface (Type, Pokemon) avec une généralité de fonctions

    9 - Oui

    10- Les classe (Carapuce, Salameche et Bulbizarre) dépendent de (TypeEau, TypeFeu, TypePlante)