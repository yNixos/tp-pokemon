import {Pokeball} from "./_Pokeball";
import {Pokemon} from "../_Interfaces/_Pokemon";

export class Dresseur {

    private _nom: string;
    public _pokeballs: Pokeball[] = [];


    constructor(nom: string) {
        this._nom = nom;
    }

    /**
     * Ajout d'une pokeball au dresseur (max: 6)
     */
    public ajouterPokeballs(){
        if (this._pokeballs.length < 6){
            this._pokeballs.push(new Pokeball(this));
            console.log('Tu viens d\'ajouter une pokeball');
        }else{
            throw new Error("Un dresseur peut avoir que 6 pokeball");
        }
    }

    /**
     * Permet de capturer un pokemon
     * @param Pokemon
     */
    public capturer(Pokemon: Pokemon): void{
        this._pokeballs.map(pokeball => {
            let i = false;
            if (!pokeball.contient){
                if (!i) {
                    if (!Pokemon._captif) {
                        pokeball.affecterPokemon(Pokemon);
                        Pokemon._captif = true;
                        i = true;
                    }
                }
            }
        });
    }

    /**
     * Permet d'afficher la liste des pokemons de chaque pokeball
     */
    public getPokemons(): void{
        try{
            this._pokeballs.map(pokeball => {console.log( ` Tu a [${pokeball.contient._nom}] comme pokemon`);});
        }catch {
            console.log('La pokeball est vide');
        }


    }


    get nom(): string {
        return this._nom;
    }

    set nom(value: string) {
        this._nom = value;
    }


    get Pokeball(): Pokeball[] {
        return this._pokeballs;
    }

    set Pokeball(value: Pokeball[]) {
        this._pokeballs= value;
    }
}