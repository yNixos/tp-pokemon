"use strict";
exports.__esModule = true;
exports.Dresseur = void 0;
var _Pokeball_1 = require("./_Pokeball");
var Dresseur = /** @class */ (function () {
    function Dresseur(nom) {
        this._pokeballs = [];
        this._nom = nom;
    }
    Dresseur.prototype.ajouterPokeballs = function () {
        if (this._pokeballs.length < 6) {
            this._pokeballs.push(new _Pokeball_1.Pokeball(this));
            console.log('Tu viens d\'ajouter une pokeball');
        }
        else {
            throw new Error("Un dresseur peut avoir que 6 pokeball");
        }
    };
    Dresseur.prototype.capturer = function (Pokemon) {
        this._pokeballs.map(function (pokeball) {
            var i = false;
            if (!pokeball.contient) {
                if (!i) {
                    if (!Pokemon._captif) {
                        pokeball.affecterPokemon(Pokemon);
                        Pokemon._captif = true;
                        i = true;
                    }
                }
            }
        });
    };
    Dresseur.prototype.getPokemons = function () {
        try {
            this._pokeballs.map(function (pokeball) { console.log(" Tu a [".concat(pokeball.contient._nom, "] comme pokemon")); });
        }
        catch (_a) {
            console.log('La pokeball est vide');
        }
    };
    Object.defineProperty(Dresseur.prototype, "nom", {
        get: function () {
            return this._nom;
        },
        set: function (value) {
            this._nom = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Dresseur.prototype, "Pokeball", {
        get: function () {
            return this._pokeballs;
        },
        set: function (value) {
            this._pokeballs = value;
        },
        enumerable: false,
        configurable: true
    });
    return Dresseur;
}());
exports.Dresseur = Dresseur;
