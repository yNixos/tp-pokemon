import {Pokemon} from "../_Interfaces/_Pokemon";
import {TypePlante} from "./_TypePlante";

export class Bulbizarre extends TypePlante implements Pokemon{

    _captif: boolean;
    _horsjeu: boolean;
    _nom: string;
    _pv: number;
    _pvMax: number;
    _type: string;


    constructor(pvMax: number, degats: number) {
        super(degats);
        this._pvMax = pvMax;
        this._nom = 'Bulbizarre';
        this._captif = false;
        this._horsjeu = false;
        this._pv = pvMax;
        this._type = 'Plante';
    }

    /**
     * Calcule les degats qui peut lui être infligé
     * @param cible
     */
    calculerDegat(cible: Pokemon): number {

        if (cible._type==='Feu'){
            return this.calculerDegatsContreFeu();
        }
        else if (cible._type==='Eau'){
            return this.calculerDegatsContreEau();
        }
        else if (cible._type==='Plante'){
            return this.calculerDegatsContrePlante();
        }
    }

    /**
     * Attaque un autre pokemon
     * @param cible
     */
    attaquer(cible: Pokemon): void {
        cible.subirAttaque(this.calculerDegat(cible));
    }

    /**
     * Mets des dégats a ton pokémon
     * @param degats
     */

    subirAttaque(degats:number): void {
        this._pv = this._pv-degats
        console.log('Votre pokémon viens de subir une attaque ces pv sont de ' + this._pv);
    }

    /**
     *
     * Permet de soigner un pokemon
     */
    soigner(): void {

        this._pv = this._pvMax;
        console.log('Vous venez de soigner votre pokemon, il a désormais ' + this._pv + ' pv !');

    }

    /**
     *
     * @param nom
     * Permet de renomer son pokemon
     */
    renommer(nom: string): void {
        this._nom = nom;
        console.log('Votre pokemon s\'apelle désormais ' + this._nom);
    }

}