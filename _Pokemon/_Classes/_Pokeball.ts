import {Pokemon} from "../_Interfaces/_Pokemon";
import {Dresseur} from "./_Dresseur";

export class Pokeball {

    private _contient!: Pokemon;
    private _proprietaire!: Dresseur;


    constructor(proprietaire: Dresseur) {
        this._proprietaire = proprietaire;
    }

    /**
     * permet de retourner le contenu de la pokeball
     */
    public getContenu(){
        return this._contient;
    }

    /**
     * Affecter un pokemon a une pokeball
     * @param pokemon
     */
    public affecterPokemon(pokemon: Pokemon){
        this.contient = pokemon;
    }


    get contient(): Pokemon {
        return this._contient;
    }

    set contient(value: Pokemon) {
        this._contient = value;
    }

    get proprietaire(): Dresseur {
        return this._proprietaire;
    }

    set proprietaire(value: Dresseur) {
        this._proprietaire = value;
    }
}