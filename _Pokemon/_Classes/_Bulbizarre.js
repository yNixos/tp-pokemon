"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
exports.Bulbizarre = void 0;
var _TypePlante_1 = require("./_TypePlante");
var Bulbizarre = /** @class */ (function (_super) {
    __extends(Bulbizarre, _super);
    function Bulbizarre(pvMax, degats) {
        var _this = _super.call(this, degats) || this;
        _this._pvMax = pvMax;
        _this._nom = 'Bulbizarre';
        _this._captif = false;
        _this._horsjeu = false;
        _this._pv = pvMax;
        _this._type = 'Plante';
        return _this;
    }
    Bulbizarre.prototype.calculerDegat = function (cible) {
        if (cible._type === 'Feu') {
            return this.calculerDegatsContreFeu();
        }
        else if (cible._type === 'Eau') {
            return this.calculerDegatsContreEau();
        }
        else if (cible._type === 'Plante') {
            return this.calculerDegatsContrePlante();
        }
    };
    Bulbizarre.prototype.attaquer = function (cible) {
        cible.subirAttaque(this.calculerDegat(cible));
    };
    Bulbizarre.prototype.renommer = function (nom) {
        this._nom = nom;
    };
    Bulbizarre.prototype.soigner = function () {
        this._pv = this._pvMax;
    };
    Bulbizarre.prototype.subirAttaque = function (degats) {
        this._pv = this._pv - degats;
    };
    return Bulbizarre;
}(_TypePlante_1.TypePlante));
exports.Bulbizarre = Bulbizarre;
