import {Type} from "../_Interfaces/_Type";
import {Pokemon} from "../_Interfaces/_Pokemon";

export abstract class TypeEau implements Type{

    _degats: number;


    constructor(degats: number) {
        this._degats = degats;
    }

    /**
     * Permet de calculer les degats de l'eau sur l'eau
     */
    calculerDegatsContreEau(): number {
        let degats =this._degats*0.5;
        return degats;
    }

    /**
     * Permet de calculer les degats de l'eau sur le feu
     */
    calculerDegatsContreFeu(): number {
        let degats =this._degats*2;
        return degats;
    }

    /**
     * Permet de calculer les degats de l'eau sur la plante
     */
    calculerDegatsContrePlante(): number {
        let degats =this._degats*0.5;
        return degats;
    }
    
}