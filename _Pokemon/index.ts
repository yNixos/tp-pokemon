import {Dresseur} from "./_Classes/_Dresseur";
import {Bulbizarre} from "./_Classes/_Bulbizarre";
import {Pokemon} from "./_Interfaces/_Pokemon";

const monDresseur : Dresseur = new Dresseur('Sasha');

monDresseur.ajouterPokeballs();
monDresseur.ajouterPokeballs();
monDresseur.ajouterPokeballs();
monDresseur.ajouterPokeballs();

const pokemon1 : Pokemon = new Bulbizarre(50, 5);
//pokemon1.renommer('T');

const pokemon2 : Pokemon = new Bulbizarre(50, 5);
pokemon2.renommer('Benjamin');

monDresseur.capturer(pokemon1);
monDresseur.capturer(pokemon2);

monDresseur.getPokemons();