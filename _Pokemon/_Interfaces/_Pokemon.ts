export interface Pokemon {

    _nom: string;
    _pv: number;
    _pvMax: number;
    _type: string;
    _captif: boolean;
    _horsjeu: boolean;

    attaquer(cible: Pokemon);
    subirAttaque(degats: number);
    soigner();
    renommer(nom: string);

}